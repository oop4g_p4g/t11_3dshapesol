#include "D3D.h"
#include "SimpleMath.h"
#include "d3dutil.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;

extern void RenderShape();
extern Matrix gWorld;

const float MAIN_SCALE = 0.75f;
float gXTrans = 0, gXVel = 1;
void TranslateUpdate(float dTime)
{
	gXTrans += gXVel * dTime;
	if (gXTrans > 3)
	{
		gXVel *= -1;
		gXTrans = 3;
	}
	else if (gXTrans < -3)
	{
		gXVel *= -1;
		gXTrans = -3;
	}
}

void TranslateRender()
{
	gWorld = Matrix::CreateScale(MAIN_SCALE) * Matrix::CreateTranslation(gXTrans, 0, 0);
	RenderShape();
}


void RotateRender(float dTime)
{
	gWorld = Matrix::CreateScale(MAIN_SCALE) * Matrix::CreateRotationZ(sinf(GetClock()*0.5f)*10.f);

	RenderShape();
}

void ScaleRender(float dTime)
{
	gWorld = Matrix::CreateScale((cosf(GetClock()*4.f)*0.5f + 1.f)*MAIN_SCALE);
	RenderShape();
}

const float LimitY = 3, LimitX = 4;
float gXtran = -LimitX, gYtran = LimitY;
int gDir = 0;
void TranslateAndRotateAndScaleUpdate(float elapsed)
{

	float speed = elapsed * 2;
	switch (gDir)
	{
	case 0:
		gXtran += speed;
		if (gXtran > LimitX)
			gDir = 1;
		break;
	case 1:
		gYtran -= speed;
		if (gYtran < -LimitY)
			gDir = 2;
		break;
	case 2:
		gXtran -= speed;
		if (gXtran < -LimitX)
			gDir = 3;
		break;
	case 3:
		gYtran += speed;
		if (gYtran > LimitY)
			gDir = 0;
		break;
	}
}

void TranslateAndRotateAndScaleRender(float dTime)
{
	float angle = sinf(GetClock()*0.5f)*10.f;
	Matrix zrot = Matrix::CreateRotationZ(angle);
	Matrix trans = Matrix::CreateTranslation(gXtran, gYtran, 0.1f);

	float sc = (cosf(GetClock()) + 2) * 0.5f * MAIN_SCALE;
	Matrix scale = Matrix::CreateScale(sc);

	gWorld = scale * zrot * trans;
	//gWorld = trans;
	//gWorld = trans * zrot * scale;

	RenderShape();
}

void MatrixUpdate(float dTime)
{
	TranslateUpdate(dTime);
	TranslateAndRotateAndScaleUpdate(dTime);
}


void MatrixRender(float dTime)
{
	TranslateRender();
	RotateRender(dTime);
	ScaleRender(dTime);
	TranslateAndRotateAndScaleRender(dTime);
}
