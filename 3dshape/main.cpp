#include <windows.h>
#include <string>
#include <cassert>
#include <d3d11.h>
#include <vector>

#include "WindowUtils.h"
#include "D3D.h"
#include "Sprite.h"
#include "ShaderTypes.h"
#include "FX.h"
#include "Game.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;


ID3D11InputLayout* gInputLayout;
ID3D11Buffer* gBoxVB;
ID3D11Buffer* gBoxIB;
ID3D11Buffer* gTriVB, *gTriIB;
ID3D11Buffer* gQuadVB, *gQuadIB;
ID3D11VertexShader* pVertexShader = nullptr;
ID3D11PixelShader* pPixelShader = nullptr;

Matrix gWorld;
Matrix gView;
Matrix gProj;

GfxParamsPerObj gGfxData;
ID3D11Buffer* gpGfxConstsPerObjBuffer;

void BuildQuad()
{
	// Create vertex buffer for a quad (two triangle square)
	VertexPosColour vertices[] =
	{
		{ Vector3(-0.5f, -0.5f, 0.f), Colors::White },
		{ Vector3(-0.5f, +0.5f, 0.f), Colors::Black },
		{ Vector3(+0.5f, +0.5f, 0.f), Colors::Red },
		{ Vector3(+0.5f, -0.5f, 0.f), Colors::Green }
	};

	CreateVertexBuffer(WinUtil::Get().GetD3D().GetDevice(),sizeof(VertexPosColour) * 4, vertices, gQuadVB);


	// Create the index buffer

	UINT indices[] = {
		// front face
		0, 1, 2,
		0, 2, 3
	};

	CreateIndexBuffer(WinUtil::Get().GetD3D().GetDevice(),sizeof(UINT) * 6, indices, gQuadIB);
}


void BuildCube()
{
	// Create vertex buffer
	VertexPosColour vertices[] =
	{	//front
		{ Vector3(-1, -1, -1),	Vector4(1, 1, 1, 1) },
		{ Vector3(-1, 1, -1),	Vector4(1, 0, 0, 1) },
		{ Vector3(1, 1, -1),	Vector4(0, 1, 1, 1) },
		{ Vector3(1, -1, -1),	Vector4(0, 0, 1, 1) },
		//back						  
		{ Vector3(-1, -1, 1),	Vector4(0, 0, 0, 1) },
		{ Vector3(-1, 1, 1),	Vector4(1, 1, 0, 1) },
		{ Vector3(1, 1, 1),		Vector4(1, 0, 1, 1) },
		{ Vector3(1, -1, 1),	Vector4(0, 1, 1, 1) }
	};
	CreateVertexBuffer(WinUtil::Get().GetD3D().GetDevice(),sizeof(VertexPosColour) * 8, vertices, gBoxVB);

	// Create the index buffer
	UINT indices[] = {
		// front face
		0, 1, 2,
		0, 2, 3,
		//right
		3,2,7,
		2,6,7,
		//left
		0,4,1,
		4,5,1,
		//top
		1,6,2,
		1,5,6,
		//bottom
		3,7,0,
		0,7,4,
		//back
		6,5,4,
		6,4,7
	};

	CreateIndexBuffer(WinUtil::Get().GetD3D().GetDevice(),sizeof(UINT) * 36, indices, gBoxIB);
}

void BuildPyramid()
{
	// Create vertex buffer
	VertexPosColour vertices[] =
	{
		{ Vector3(-1, -1, -1), Vector4(1, 0, 1, 1) },
		{ Vector3(0, -1, 1), Vector4(1, 0, 0, 1) },
		{ Vector3(1, -1, -1), Vector4(0, 1, 1, 1) },
		{ Vector3(0, 1, 0), Vector4(1, 1, 0, 1) }
	};
	CreateVertexBuffer(WinUtil::Get().GetD3D().GetDevice(),sizeof(VertexPosColour) * 4, vertices, gTriVB);

	// Create the index buffer
	UINT indices[] = {
		0,3,2,
		2,3,1,
		1,3,0,
		0,2,1
	};

	CreateIndexBuffer(WinUtil::Get().GetD3D().GetDevice(),sizeof(UINT) * 12, indices, gTriIB);
}


void BuildGeometryBuffers()
{
	BuildQuad();
	BuildCube();
	BuildPyramid();
}

void UpdateConstsPerObj()
{
	gGfxData.wvp = gWorld * gView * gProj;
	gGfxData.clocks.x = GetClock();
	WinUtil::Get().GetD3D().GetDeviceCtx().UpdateSubresource(gpGfxConstsPerObjBuffer, 0, nullptr, &gGfxData, 0, 0);
}

bool BuildFX()
{
	MyD3D& d3d = WinUtil::Get().GetD3D();
	CheckShaderModel5Supported(d3d.GetDevice());

	// Create the constant buffers for the variables defined in the vertex shader.
	CreateConstantBuffer(d3d.GetDevice(),sizeof(GfxParamsPerObj), &gpGfxConstsPerObjBuffer);

	//load in a pre-compiled vertex shader
	char* pBuff = nullptr;
	unsigned int bytes = 0;
	pBuff = ReadAndAllocate("../bin/data/SimpleVS.cso", bytes);
	CreateVertexShader(d3d.GetDevice(),pBuff, bytes, pVertexShader);
	//create a link between our data and the vertex shader
	CreateInputLayout(d3d.GetDevice(),VertexPosColour::sVertexDesc, 2, pBuff, bytes, &gInputLayout);
	delete[] pBuff;

	//load in a pre-compiled pixel shader	
	pBuff = ReadAndAllocate("../bin/data/SimplePS.cso", bytes);
	CreatePixelShader(d3d.GetDevice(),pBuff, bytes, pPixelShader);
	delete[] pBuff;


	return true;

}


void InitGame()
{
	BuildGeometryBuffers();
	BuildFX();

	gWorld = Matrix::Identity;
	CreateProjectionMatrix(gProj, 0.25f*PI, WinUtil::Get().GetAspectRatio(), 1, 1000.f);
	CreateViewMatrix(gView, Vector3(0, 0, -10), Vector3(0, 0, 0), Vector3(0, 1, 0));
}

void ReleaseGame()
{
	ReleaseCOM(pVertexShader);
	ReleaseCOM(pPixelShader);
	ReleaseCOM(gpGfxConstsPerObjBuffer);
	ReleaseCOM(gBoxVB);
	ReleaseCOM(gBoxIB);
	ReleaseCOM(gTriVB);
	ReleaseCOM(gTriIB);
	ReleaseCOM(gQuadVB);
	ReleaseCOM(gQuadIB);
	ReleaseCOM(gInputLayout);
}



void RenderShape(ID3D11Buffer *pVB, ID3D11Buffer *pIB, int numIndices)
{
	MyD3D& d3d = WinUtil::Get().GetD3D();
	d3d.InitInputAssembler(gInputLayout, pVB, sizeof(VertexPosColour), pIB);
	d3d.GetDeviceCtx().VSSetShader(pVertexShader, nullptr, 0);
	d3d.GetDeviceCtx().PSSetShader(pPixelShader, nullptr, 0);
	UpdateConstsPerObj();
	d3d.GetDeviceCtx().VSSetConstantBuffers(0, 1, &gpGfxConstsPerObjBuffer);
	d3d.GetDeviceCtx().DrawIndexed(numIndices, 0, 0);
}

void RenderShape()
{
	RenderShape(gTriVB, gTriIB, 12);
	gWorld *= Matrix::CreateTranslation(1, 0, 0);
	RenderShape(gBoxVB, gBoxIB, 36);
	//RenderShape(gQuadVB, gQuadIB, 6);
}


void Render(float dTime)
{
	MyD3D& d3d = WinUtil::Get().GetD3D();
	d3d.BeginRender(Colours::Black);

	MatrixRender(dTime);

	d3d.EndRender();
}

LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	const float camInc = 0.1f;

	//do something game specific here
	switch (msg)
	{
		// Respond to a keyboard event.
	case WM_CHAR:
		switch (wParam)
		{
		case 27:
			PostQuitMessage(0);
			return 0;
		}
	}

	//default message handling (resize window, full screen, etc)
	return WinUtil::Get().DefaultMssgHandler(hwnd, msg, wParam, lParam);
}

void OnResize(int screenWidth, int screenHeight, MyD3D& d3d)
{
	CreateProjectionMatrix(gProj, 0.25f*PI, WinUtil::Get().GetAspectRatio(), 1, 1000.f);
	d3d.OnResize_Default(screenWidth, screenHeight);
}

//main entry point for the game
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
				   PSTR cmdLine, int showCmd)
{

	int w(512), h(256);
	//int defaults[] = { 640,480, 800,600, 1024,768, 1280,1024 };
		//WinUtil::ChooseRes(w, h, defaults, 4);
	if (!WinUtil::Get().InitMainWindow(w, h, hInstance, "Fezzy", MainWndProc, true))
		assert(false);

	MyD3D d3d;
	if (!d3d.InitDirect3D(OnResize))
		assert(false);
	WinUtil::Get().SetD3D(d3d);
	d3d.GetCache().SetAssetPath("data/");

	InitGame();

	bool canUpdateRender;
	float dTime = 0;
	while (WinUtil::Get().BeginLoop(canUpdateRender))
	{
		if (canUpdateRender && dTime>0)
		{
			MatrixUpdate(dTime);
			Render(dTime);
		}
		dTime = WinUtil::Get().EndLoop(canUpdateRender);
	}

	d3d.ReleaseD3D(true);	
	return 0;
}

